<?php
require_once 'connect.php';
error_reporting(E_ERROR);

$response = [
    "status" => false
];

if($_SERVER["REQUEST_METHOD"]== "POST") {
    if (!empty($_POST['id'])) {
        $id = $_POST['id'];
        $delete_file=$link->prepare("SELECT `avatar` FROM `general` WHERE `general`.`id` = ?");
        $delete_file->execute([$id]);
        $delete_file=$delete_file->fetchAll();
        unlink($delete_file[0]['avatar']);

    $delete_general=$link->prepare("DELETE FROM `general` WHERE `general`.`id` = ?");
    $delete_general->execute([$id]);

    if ($delete_general && !is_string($delete_general)) {
        $response = [
            "status" => true
        ];
    }

}
};
echo json_encode($response); // чтобы преобразовать php массив в json
<?php
error_reporting(E_ERROR);
require_once('connect.php');
$table='<tr>
<th scope="col" >Id</th>
<th scope="col-2" >Имя</th>
<th scope="col-2" >Родитель</th>
<th scope="col" >Дата рождения</th>
<th scope="col" >Группа</th>
<th scope="col" >Кол-во пропусков в месяце</th>
<th >Фото</th>
</tr>';
/* Делаем выборку всех строк из таблицы "general" */
    $general = $link->prepare( "SELECT * FROM `general`");
    $general->execute();
    $general=$general->fetchAll();
    /*
        * Перебираем массив и рендерим HTML с данными из массива
    */
        foreach ($general as $general)
        {
            $table.='
            <tr>
            <td scope="row">'. $general['id'].'</td>
            <td>'.$general['name'].'</td>
            <td>'.$general['parent']. '</td>
            <td>'.$general['birthday'].'</td>
            <td>'.$general['num_group'].'</td>
            <td>'.$general['pass'].'</td>
            <td><img src="'.$general['avatar'].'"  class="img-fluid"></td>
            <td><button class="edit_data btn btn-primary" data-toggle="modal" data-target="#updateForm" data-id="'. $general['id'] .'" > Изменить</button></td>
            <td><button class="delete_data btn btn-danger" data-toggle="modal" data-target="#deleteForm" data-id="'. $general['id'] .'" > Удалить</button></td>
            <td><button class="info_pay btn btn-outline-info" data-toggle="modal" data-target="#infoPaymentsForm" data-id="'. $general['id'] .'" > Платежи </button></td>
            </tr>';
        }
echo $table;
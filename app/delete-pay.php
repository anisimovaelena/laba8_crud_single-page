<?php
require_once 'connect.php';
error_reporting(E_ERROR);

$response = [
    "status" => false
];

if($_SERVER["REQUEST_METHOD"]== "POST") {
    if (!empty($_POST['pay_id'])) {
        $id = $_POST['pay_id'];

    $delete_payment=$link->prepare("DELETE FROM `payment` WHERE `payment`.`id_pay` = ?");
    $delete_payment->execute([$id]);

    if ($delete_payment && !is_string($delete_payment)) {
        $response = [
            "status" => true ];
        }
    }
}

echo json_encode($response); // чтобы преобразовать php массив в json
<?php
error_reporting(E_ERROR);
require_once('connect.php');
$table='
<tr>
    <th scope="col" >Номер</th>
    <th scope="col-2" >Имя</th>
    <th scope="col-2" >Сумма</th>
    <th scope="col-3" >Дата </th>
</tr>
';
/* 
    * Делаем выборку всех строк из таблицы "payment" и добавляем к ним связанные записи из таблицы с детьми 
*/
    $pay=$link->prepare("SELECT * FROM payment INNER JOIN general ON payment.id_name = general.id ORDER BY payment.id_pay");
    $pay->execute();
    $pay=$pay->fetchAll();

/*
    * Перебираем массив и рендерим HTML с данными из массива
*/
    foreach ($pay as $pay) 
    {
        $table.='<tr>
        <td scope="row" >'.$pay['id_pay'].'</td>
            <td><a class="info" data-toggle="modal" data-target="#infoKidForm" data-id="'.$pay['id_name'].'"> '.$pay['name'].' </a> </td>
            <td>'.$pay['money'].'</td>
            <td>'.$pay['date'].'</td>
            <td><button class="edit-pay btn btn-primary" data-toggle="modal" data-target="#updatePayForm" data-id="'.$pay['id_pay'].'" > Изменить </button></td>
            <td><button class="delete-pay btn btn-danger" data-toggle="modal" data-target="#deletePayForm" data-id="'.$pay['id_pay'].'"> Удалить </button></td>
        </tr>';
    }
echo $table;
    
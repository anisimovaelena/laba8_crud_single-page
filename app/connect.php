<?php
$dbhost = "localhost";
$dbname = "test";
$dbusername = "root";
$dbpassword = "";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    //PDO::ATTR_EMULATE_PREPARES   => false,
];
    $dsn="mysql:host=$dbhost;dbname=$dbname";
    try {
        $link = new PDO($dsn, $dbusername, $dbpassword, $opt);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
    }
    


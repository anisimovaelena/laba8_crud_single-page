<?php
session_start();
if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
}
require_once 'connect.php';
error_reporting(E_ERROR);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">
    

</head>
<body>


<?php require_once('header.php'); 

?>
<div class="container">
<a class="btn btn-dark" href="table-pay.php" role="button">Платежи</a> <br><br>
        <table class="table thead-light">
        </table>
 
    <div class="row">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#createForm" > Добавить </button>
    </div>
</div>

<div class="modal fade" id="infoPaymentsForm" tabindex="-1" role="dialog" aria-labelledby="infoFormLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infoFormLabel">Платежи :</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container">
      <div class="message alert alert-warning" style="display:none" ></div>
            <div class="related-payments">
            </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="createForm" tabindex="-1" role="dialog" aria-labelledby="createForm" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить ребёнка</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-5">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" name="name"> </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-5">
                            <label for="parent">Родитель</label>
                            <input class="form-control" type="name" name="parent"> </div>
                        </div>
                    <div class="form-group row">
                    <div class="col-5">
                        <label for="birthday">Дата рождения</label>
                        <input class="form-control" type="date" name="birthday"> </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-5">
                        <label for="num_group">Номер группы</label>
                        <input class="form-control" type="number" name="num_group"> </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-5">
                        <label for="pass">Кол-во пропусков в месяце</label>
                        <input class="form-control" type="number" name="pass"> </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-5">
                        <label for="avatar">Фото</label>
                        <input class="form-control-file" type="file" name="avatar"> </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="create-btn btn btn-primary" data-dismiss="modal">Добавить</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="updateForm" tabindex="-1" role="dialog" aria-labelledby="updateForm" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Изменить данные ребёнка</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>  </button>
        </div>
        <div class="modal-body">
            <form enctype="multipart/form_data">
                <div class="form-group row"> <input type="hidden" name="update_id"> </div>
                        <div class="form-group row">
                            <div class="col-5">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" name="update_name" > </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                                <label for="parent">Родитель</label>
                                <input class="form-control" type="name" name="update_parent" > </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                            <label for="birthday">Дата рождения</label>
                            <input class="form-control" type="date" name="update_birthday" > </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                            <label for="num_group">Номер группы</label>
                            <input class="form-control" type="number" name="update_num_group" > </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                            <label for="pass">Кол-во пропусков в месяце</label>
                            <input class="form-control" type="number" name="update_pass" > </div>
                        </div>
                        <label for="avatar">Фото</label>
                        <div class="form-check ">
                        <input class="form-check-input" type="checkbox" value="" id="delete_checkbox">
                            <label class="form-check-label" for="">
                                Удалить фото
                            </label>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                            <img id="img-to-edit" scr="..." class="img-fluid" style="cursor:pointer" title="изменить аватар">
                            <input class="form-control-file" type="file" id="update_avatar" name="update_avatar"  > </div>
                        </div> 
                
            </form>
        </div>
        <div class="modal-footer">
            <button type="submit" class="update-btn btn btn-primary" data-dismiss="modal">Изменить</button>
        </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteForm" tabindex="-1" role="dialog" aria-labelledby="deleteForm" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Удалить</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="delete_id"> 
                Вы действительно хотите удалить запись?
            </div>
        
        <div class="modal-footer">
            <button type="submit" class="del-btn btn btn-secondary" data-dismiss="modal">Удалить</button>
        </div>
        </div>
    </div>
</div>


<br>
<br>
    <div>
        <a class="btn btn-warning" href="logout.php"> Выход </a>
    </div>



<?php require_once('footer.php');?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="../js/show-tables.js" ></script>
<script src="../js/show-related.js" ></script>
<script src="../js/create.js" ></script>
<script src="../js/update.js" ></script>
<script src="../js/delete.js" ></script>
<script>
    showKids();
</script>
</body>
</html>
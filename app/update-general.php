<?php
session_start();
require_once 'connect.php';
error_reporting(E_ERROR);
$response = [
    "status" => false
];

if($_SERVER["REQUEST_METHOD"]== "POST") {
    //print_r($_POST);
    $id = $_POST['id'];
    $name = $_POST['name']; 
    $parent=$_POST['parent'];
    $birthday = $_POST['birthday'];
    $num_group = $_POST['num_group'];
    $pass = $_POST['pass'];
    $delete =  $_POST['delete'];

    $del_file=$link->prepare("SELECT `avatar` FROM `general` WHERE `general`.`id` = ? ");
    $del_file->execute([$id]);
    $del_file=$del_file->fetchAll();
    //print_r($del_file);
    
    if ($delete) 
    {
        //echo "Delete is true";
        $path="";
        unlink($del_file[0]['avatar']);
    } 
    else {
        define("UPLOAD_DIR", "../uploads/");
        if (!empty($_FILES["avatar"])) {
            echo "FILES не пуст";
            $avatar = $_FILES["avatar"];

            if ($avatar["error"] !== UPLOAD_ERR_OK) {
                echo "<p>Произошла ошибка.</p>";
                exit;
            }

            // обеспечиваем безопасное имени файла
            $name_file = preg_replace("/[^A-Z0-9._-]/i", "_", $avatar["name"]);

            // не перезаписываем существующий файл
            $i = 0;
            $parts = pathinfo($name_file);
            while (file_exists(UPLOAD_DIR . $name_file)) {
                $i++;
                $name_file = $parts["filename"] . "-" . $i . "." . $parts["extension"];
            }

            // сохраняем файл из временного каталога
            $success = move_uploaded_file($avatar["tmp_name"],
                UPLOAD_DIR . $name_file);
            if (!$success) { 
                echo "<p>Не удалось сохранить файл.</p>";
                exit;
            }

            // устанавливаем правильные права для нового файла
            chmod(UPLOAD_DIR . $name_file, 0644);
            $path=UPLOAD_DIR.$name_file;
            unlink($del_file[0]['avatar']);
        }
    }
    $upd_general=$link->prepare("UPDATE `general` SET `name` = ?, `parent` = ?, `birthday` = ?, `num_group` = ?, `pass` = ?, `avatar` = ? WHERE `general`.`id` = ?");
    $upd_general->execute([$name, $parent, $birthday, $num_group, $pass, $path, $id]);


if ($upd_general && !is_string($upd_general)) {
    $response = [
        "status" => true
    ];
}
}
echo json_encode($response); // чтобы преобразовать php массив в json

<?php
require_once 'connect.php';
/*Получаем инфу о платежах */
error_reporting(E_ERROR);
$response = [
    "status" => false,
    "message" => "Связанные платежи не найдены"
];
if($_SERVER["REQUEST_METHOD"]== "POST") {
    if (!empty($_POST['id'])) {
        $id=$_POST['id'];
        $related_pay=$link->prepare("SELECT * FROM `payment` WHERE `id_name`=?");
        $related_pay->execute([$id]);
        $related_pay=$related_pay->fetchAll();
        //print_r($related_pay);
        $list='';
        if (count($related_pay)!=0) {
            foreach ($related_pay as $related_pay) {
                $list.='<div class="row">
                <ul class="list-group list-group-flush">
                <li class="id list-group-item"> <h3>Id платежа: '.$related_pay['id_pay'].' </h3> </li>
                <li class="money list-group-item"> Сумма платежа: '.$related_pay['money'].'</li>
                <li class="date list-group-item"> Дата: '.$related_pay['date'].'</li>
                </ul>
                </div>';
            }
            $response = [
                "status" => true,
                "message" => $list
            ];
        }
    }
}
echo json_encode($response);
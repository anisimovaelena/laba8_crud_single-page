$('#deleteForm').on('shown.bs.modal',function (event) {
    var button = $(event.relatedTarget); 
    let id = button.data('id');
    var modal = $(this);
    $('input[name="delete_id"]').val(id); 

});

$('.del-btn').click(function(e) {
    e.preventDefault(); 
    let id=$('input[name="delete_id"]').val(); 

    $.ajax({
        url: 'delete-general.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        data: {
            id: id
        },
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success (result) { 
            if (result.status) {
                showKids();
            }
        }
    })
});

$('#deletePayForm').on('shown.bs.modal',function (event) {
    var button = $(event.relatedTarget); 
    let id = button.data('id');
    var modal = $(this);;
    $('input[name="pay_id"]').val(id); 

});

$('.del-pay-btn').click(function(e) {
    e.preventDefault(); 
    let pay_id=$('input[name="pay_id"]').val(); 

    $.ajax({
        url: 'delete-pay.php',
        type: 'POST',
        dataType: 'json',
        data: { 
            pay_id: pay_id
        },
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success (result) { 
            if (result.status) {
                showPayments();
            }
        }
    })
});
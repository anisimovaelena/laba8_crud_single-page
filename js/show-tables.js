function showKids(){
    $.ajax({
        url: "show-general.php",
        dataType:"html",
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success:function(data){
            $('.table').html(data);
        }
    });
}
function showPayments(){
    $.ajax({
        url: "show-payment.php",
        dataType:"html",
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success:function(data){
            $('.table').html(data);
        }
    });
}
function selectforPayments() {
    $.ajax({
        url: "select-for-payment.php",
        dataType:"html",
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success:function(data){
            $('.custom-select').html(data);
        }
    });
}
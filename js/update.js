/* 
    Валидация для формы редактирования
*/

$('#updateForm').on('shown.bs.modal',function (event) {
    var button = $(event.relatedTarget); 
    let id = button.data('id');
    var modal = $(this);
    $.ajax({
        url: "general-by-id.php",
        method:"POST",
        data:
        {
            id:id
        },
        dataType:"json",
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success:function(data){
             modal.find('input[name="update_id"]').val(data[0].id); 
             modal.find('input[name="update_name"]').val(data[0].name); 
             modal.find('input[name="update_parent"]').val(data[0].parent);
             modal.find('input[name="update_birthday"]').val(data[0].birthday);
             modal.find('input[name="update_num_group"]').val(data[0].num_group);
             modal.find('input[name="update_pass"]').val(data[0].pass);
             modal.find('#img-to-edit').attr("src", data[0].avatar);
            let src=$('#img-to-edit').attr("src");
            if (src!="...")
            {
                $('input[type="checkbox"]').attr("style", "");
                $("#update_avatar").attr("style", "display:none");
            }
            else 
            {
                $('input[type="checkbox"]').attr("style", "display:none");
                $("#update_avatar").attr("style", "");
            }
        }
    });
});


$('input[name="update_avatar"]').change(function (e) {
    avatar = e.target.files[0];
    $("#img-to-edit").attr("style", "display:none");
    $("#update_avatar").attr("style", "");
});
$('#delete_checkbox').click(function(e){
    if (this.checked) //
       { 
           $("#img-to-edit").attr("style", "display:none");
           $("#update_avatar").attr("style", "display:none");
       }
    else {
            $("#img-to-edit").attr("style", "");
       }
});
$("#img-to-edit").click(function () {
    $("#update_avatar").trigger('click');
});

$('.update-btn').click(function(e) {
    e.preventDefault(); // отменяет действие браузера по умолчанию
    let id = $('input[name="update_id"]').val(); 
    let name = $('input[name="update_name"]').val(); 
    let parent = $('input[name="update_parent"]').val();
    let birthday = $('input[name="update_birthday"]').val();
    let num_group = $('input[name="update_num_group"]').val();
    let pass = $('input[name="update_pass"]').val();
    let delete_avatar = $('#delete_checkbox').prop('checked');

    let formData = new FormData();
        formData.append('id', id);
        formData.append('name', name);
        formData.append('parent', parent);
        formData.append('birthday', birthday);
        formData.append('num_group', num_group);
        formData.append('pass', pass);
        formData.append('delete', delete_avatar);
        formData.append('avatar', avatar);
    
    if (name === '')  {
        $('input[name="update_name"]').addClass("is-invalid");
        return false;
    } 
    if (parent === '') {
        $('input[name="update_name"]').addClass("is-invalid");
        return false;
    } 
    if (birthday === '') {
        $('input[name="update_birthday"]').addClass("is-invalid");
        return false;
        }
    if (num_group === '') {
        $('input[name="update_num_group"]').addClass("is-invalid");
        return false;
        }
    if (pass === '') {
        $('input[name="update_pass"]').addClass("is-invalid");
        return false;
        }

    $.ajax({
        url: 'update-general.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,

        success (result) { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)
            if (result.status) { 
                showKids();
            }
        }
    })
});

/* 
    Для payment
*/
$('#updatePayForm').on('shown.bs.modal',function (event) {
    var button = $(event.relatedTarget); 
    let id = button.data('id');
    var modal = $(this);
    $.ajax({
        url: "payment-by-id.php",
        method:"POST",
        data:
        {
            id:id
        },
        dataType:"json",

        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success:function(data){
            $('input[name="update_id"]').val(data[0]['id_pay']);
            $('#sel option:contains("'+data[1][0]['name']+'")').prop('selected', true);
            $('input[name="update_money"]').val(data[0]['money']);
            $('input[name="update_date"]').val(data[0]['date']);
        }
    });
});

$('.update-pay-btn').click(function(e) {
    let id = $('input[name="update_id"]').val(); 
    let id_kid = $('#sel option:selected').val();
    let money = $('input[name="update_money"]').val(); 
    let date = $('input[name="update_date"]').val();
    
    if (id_kid === '') {
        $('#select').addClass("is-invalid");
        return false;
        } 
    if (money === '') {
        $('input[name="update_money"]').addClass("is-invalid");
        return false;
        }
    if (date === '') {
        $('input[name="update_date"]').addClass("is-invalid");
        return false;
        } 

    $.ajax({
        url: 'update-payment.php', 
        type: 'POST',
        dataType: 'json',
        data: { 
            id: id,
            id_kid: id_kid,
            money: money,
            date: date,
        },
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success (result) { 
            if (result.status) {
                showPayments();
                }
        }
    })
});
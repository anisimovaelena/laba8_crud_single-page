$('#infoPaymentsForm').on('shown.bs.modal',function (event) {
    var button = $(event.relatedTarget); 
    let id = button.data('id');
    var modal = $(this);
    $.ajax({
        url: "related-payment.php",
        method:"POST",
        data:
        {
            id:id
        },
        dataType:"json",
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success:function(response){
            if (response.status) {
                modal.find('.message').attr("style", "display:none");
                modal.find('.related-payments').html(response.message);
                
            }
            else {
                modal.find('.message').text(response.message);
                modal.find('.message').attr("style", "");
                modal.find('.related-payments').html("");
            }

        }
    });
  })



  $('#infoKidForm').on('shown.bs.modal',function (event) {
    var button = $(event.relatedTarget); 
    let id = button.data('id');
    var modal = $(this);
    $.ajax({
        url: "general-by-id.php",
        method:"POST",
        data:
        {
            id:id
        },
        dataType:"json",
        error: function(xhr, status, error) {
            alert(xhr.responseText + '|\n' + status + '|\n' +error);
        },
        success:function(data){

            $('img').attr("src", data[0].avatar);
            $('h3.name').text(data[0].name); 
            $('.parent').text("Родитель: "+data[0].parent); 
            $('.birthday').text("Дата рождения: "+data[0].birthday);
            $('.num_group').text("Номер группы: "+data[0].num_group); 
            $('.pass').text("Кол-во пропусков в этом месяце: "+data[0].pass); 

        }
    });
  })